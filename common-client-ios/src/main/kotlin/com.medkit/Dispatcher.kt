package com.medkit

import kotlinx.coroutines.*
import kotlin.coroutines.*
import platform.darwin.*
import kotlin.native.concurrent.*

internal actual val UIDispatcher: CoroutineDispatcher = NsQueueDispatcher(dispatch_get_main_queue())

internal actual val IODispatcher: CoroutineDispatcher = NsQueueDispatcher(dispatch_get_main_queue())

internal class NsQueueDispatcher(private val dispatchQueue: dispatch_queue_t) : CoroutineDispatcher() {
    override fun dispatch(context: CoroutineContext, block: Runnable) {
        dispatch_async(dispatchQueue) { block.run() }
    }
}