package com.medkit.presentation.memes

import com.medkit.DispatcherProvider
import com.medkit.data.model.Meme
import com.medkit.data.repository.MemeRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MemesPresenter(
        private val repository: MemeRepository,
        private val dp: DispatcherProvider
) : MemesContract.Presenter {

    private lateinit var view: MemesContract.View
    private var memes: List<Meme> = listOf()
    private var currentMeme: Int = 0

    override fun start(view: MemesContract.View) {
        this.view = view
        initView()
    }

    override fun onNextClicked() {
        showNextMemeIfExists()
    }

    private fun initView() {
        getNewMemes()
        currentMeme = 0
        showNextMemeIfExists()
    }

    private fun getNewMemes() {
        GlobalScope.launch(dp.IO) {
            memes = repository.getMemesFromServer()
            launch(dp.UI) { showCurrentMemeIfExists() }
        }
    }

    private fun showCurrentMemeIfExists() {
        if (memes.isEmpty()) return
        view.showMemeImage(memes[currentMeme].url)
        view.showMemeTitle(memes[currentMeme].title)
    }

    private fun showNextMemeIfExists() {
        if (memes.isEmpty()) return
        currentMeme = (currentMeme + 1) % memes.size
        showCurrentMemeIfExists()
    }
}