package com.medkit.data.repository

import com.medkit.data.api.ApiUtils
import com.medkit.data.model.Meme
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.http.URLProtocol
import kotlinx.serialization.ImplicitReflectionSerializer
import kotlinx.serialization.json.JsonTreeParser

class MemeRepository {

    private val client = HttpClient()

    @UseExperimental(ImplicitReflectionSerializer::class)
    suspend fun getMemesFromServer(): List<Meme> {
        val response: String = client.get {
            url {
                protocol = URLProtocol.HTTP
                host = ApiUtils.host
                port = 8080
                encodedPath = "memes"
            }
        }

        return parseResultToMemes(response)
    }

    // TODO: This should be refactored as soon as kotlinx serialization is available for Kotlin Native
    private fun parseResultToMemes(result: String): List<Meme> {
        val elem = JsonTreeParser(result).readFully()
        return elem.jsonArray.map {
            Meme(it.jsonObject["title"]!!.primitive.content, it.jsonObject["url"]!!.primitive.content)
        }
    }
}