package com.medkit

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers.Unconfined

internal expect val UIDispatcher: CoroutineDispatcher

internal expect val IODispatcher: CoroutineDispatcher

interface DispatcherProvider {
    val UI: CoroutineDispatcher
    val IO: CoroutineDispatcher
}

class DispatcherProviderDefault : DispatcherProvider {
    override val UI = UIDispatcher
    override val IO = IODispatcher
}

class DispatcherProviderTest : DispatcherProvider {
    override val UI = Unconfined
    override val IO = Unconfined
}