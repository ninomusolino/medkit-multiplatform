package com.medkit

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.medkit.data.repository.MemeRepository
import com.medkit.presentation.memes.MemesContract
import com.medkit.presentation.memes.MemesPresenter
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_memes.*

class MemesActivity : AppCompatActivity(), MemesContract.View {

    private val presenter = MemesPresenter(
            MemeRepository(),
            DispatcherProviderDefault()
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_memes)
        nextView.setOnClickListener { presenter.onNextClicked() }
        presenter.start(this)
    }

    override fun showMemeImage(url: String) {
        Glide.with(this).load(url).into(memeView)
    }

    override fun showMemeTitle(title: String) {
        titleView.text = title
    }
}
