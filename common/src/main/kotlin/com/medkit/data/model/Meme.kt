package com.medkit.data.model

import kotlinx.serialization.Serializable

@Serializable
data class Meme(
        val title: String,
        val url: String
)