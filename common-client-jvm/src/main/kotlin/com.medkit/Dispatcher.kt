package com.medkit

import kotlinx.coroutines.*

internal actual val UIDispatcher: CoroutineDispatcher = Dispatchers.Main

internal actual val IODispatcher: CoroutineDispatcher = Dispatchers.IO