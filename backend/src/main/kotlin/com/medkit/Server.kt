package com.medkit

import com.medkit.controllers.meme
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.features.*
import io.ktor.gson.GsonConverter
import io.ktor.http.ContentType
import io.ktor.routing.Routing
import org.slf4j.event.Level

fun Application.module() {
    install(DefaultHeaders)
    install(ContentNegotiation) {
        register(ContentType.Application.Json, GsonConverter(buildGson()))
    }
    install(Compression)
    install(CallLogging){
        level = Level.TRACE
    }

    install(CORS)

    install(Routing) {
        meme()
    }
}

private fun buildGson(): Gson {
    return GsonBuilder()
            .setPrettyPrinting()
            .serializeNulls()
            .create()
}